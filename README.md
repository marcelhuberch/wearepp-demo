###############################################################
# Tapestry 5.4.1 Demo Project from Marcel Huber
###############################################################

git clone https://marcelhuberch@bitbucket.org/marcelhuberch/weareapp-demo.git

mvn jetty:run

http://localhost:8080/weareapp

userName: user

password: 1234

have fun ;)

*******

# Docker Image mit Maven:

Docker Image mit Maven erstellen:

mvn package docker:build

docker run -d -p 8080:8080 --name weareapp marcelhuber/weareapp

*******

# Docker Image in Einzelschritten:

mvn clean install

docker build -t marcelhuber/weareapp .

docker run --rm -it -p 8080:8080 --name weareapp marcelhuber/weareapp

oder

docker run -d -p 8080:8080 --name weareapp marcelhuber/weareapp

share logs

docker run -d -p 8080:8080 -v ~/Documents/tomcat-logs:/usr/local/tomcat/logs --name weareapp marcelhuber/weareapp

*******

# Tapestry Docs:

http://tapestry.apache.org/index.html

http://tapestry.apache.org/getting-started.html

http://tapestry.apache.org/documentation.html

http://tapestry.apache.org/request-processing.html

http://jumpstart.doublenegative.com.au/jumpstart7/

https://books.google.ch/books?id=tqJyvAoS-9kC&printsec=frontcover&dq=tapestry+5&cd=2%23v%3Donepage&q=&hl=de#v=onepage&q=tapestry%205&f=false