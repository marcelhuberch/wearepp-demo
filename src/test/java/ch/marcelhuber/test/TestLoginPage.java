package ch.marcelhuber.test;

import java.util.HashMap;
import java.util.Map;

import org.apache.tapestry5.dom.Document;
import org.apache.tapestry5.dom.Element;
import org.apache.tapestry5.test.PageTester;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * mvn test
 * 
 * @author marcelhuber
 *
 */
public class TestLoginPage extends Assert {

	private PageTester pageTester;

	@BeforeClass
	public void setUp() {
		pageTester = new PageTester("ch.marcelhuber", "app", "src/main/webapp");
	}

	@Test
	public void test1LoginPage() {
		Document doc = pageTester.renderPage("Login");
		assertTrue(doc.toString().contains("<title>Login</title>"));
	}

	@Test
	public void test2Login() {
		Document doc = pageTester.renderPage("Login");
		Element loginForm = doc.getElementById("form");
		
		assertNotNull(loginForm);

		// Fail UserName
		Map<String, String> fieldValues = new HashMap<String, String>();
		fieldValues.put("userName", "blabla");
		fieldValues.put("password", "1234");
		doc = pageTester.submitForm(loginForm, fieldValues);
		assertTrue(doc.toString().contains("UserName or Password wrong!"));
		
		// Fail Password
		fieldValues = new HashMap<String, String>();
		fieldValues.put("userName", "user");
		fieldValues.put("password", "wrong");
		doc = pageTester.submitForm(loginForm, fieldValues);
		assertTrue(doc.toString().contains("UserName or Password wrong!"));
		
		// Success
		fieldValues = new HashMap<String, String>();
		fieldValues.put("userName", "user");
		fieldValues.put("password", "1234");
		doc = pageTester.submitForm(loginForm, fieldValues);
		assertTrue(doc.toString().contains("<title>weareapp Index</title>"));
	}

	@AfterClass
	public void shutdown() {
		pageTester.shutdown();
	}
}
