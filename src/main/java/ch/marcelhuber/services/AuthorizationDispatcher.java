package ch.marcelhuber.services;

import java.io.IOException;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.internal.EmptyEventContext;
import org.apache.tapestry5.services.ApplicationStateManager;
import org.apache.tapestry5.services.Cookies;
import org.apache.tapestry5.services.Dispatcher;
import org.apache.tapestry5.services.PageRenderRequestHandler;
import org.apache.tapestry5.services.PageRenderRequestParameters;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Response;

import ch.marcelhuber.model.User;
import ch.marcelhuber.model.User.Role;
import ch.marcelhuber.pages.Index;
import ch.marcelhuber.pages.Login;
import ch.marcelhuber.utils.WeAreAppConstantLibrary;
import ch.marcelhuber.utils.WeAreAppUtil;

/**
 * Authorization Dispatcher
 * 
 * @author Marcel Huber
 * @version 1.0
 * 
 */
public class AuthorizationDispatcher implements Dispatcher {

	private PageRenderRequestHandler requestHandler;
	private ApplicationStateManager applicationStateManager;
	private Cookies cookies;

	public AuthorizationDispatcher(final PageRenderRequestHandler handler, final ApplicationStateManager applicationStateManager, final Cookies cookies) {
		super();
		this.requestHandler = handler;
		this.applicationStateManager = applicationStateManager;
		this.cookies = cookies;
	}

	public boolean dispatch(final Request request, final Response response) throws IOException {
		
		final String loginPageName = Login.class.getSimpleName();
		final String indexPageName = Index.class.getSimpleName();
		final String path = request.getPath().toLowerCase(new Locale("de"));
		
		if ( ! path.contains("login")) {
			
			User user = this.applicationStateManager.getIfExists(User.class);
			if (user == null) {				
				final String username = cookies.readCookieValue(WeAreAppConstantLibrary.COOKIE_USER);
				if(StringUtils.isNotBlank(username)) {
					if (WeAreAppUtil.getMD5Hash("user").equals(username)) {
						user = new User();
						user.setUserName("user");
						user.setPassword("1234");
						user.setRole(Role.USER);
						applicationStateManager.set(User.class, user);
					}
					if (WeAreAppUtil.getMD5Hash("admin").equals(username)) {
						user = new User();
						user.setUserName("admin");
						user.setPassword("admin");
						user.setRole(Role.ADMIN);
						applicationStateManager.set(User.class, user);
					}
				}
			}
			if (path.contains("/admin") && user != null && ! user.getRole().name().equals(Role.ADMIN.name())) {
				final PageRenderRequestParameters parameters = new PageRenderRequestParameters(indexPageName, new EmptyEventContext(), false);
				this.requestHandler.handle(parameters);
				return true;
			}
			if (user == null) {
				final PageRenderRequestParameters parameters = new PageRenderRequestParameters(loginPageName, new EmptyEventContext(), false);
				this.requestHandler.handle(parameters);
				return true;
			}
		}
		return false;
	}
}