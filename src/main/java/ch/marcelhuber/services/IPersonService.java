package ch.marcelhuber.services;

import java.util.List;

import ch.marcelhuber.model.Person;

public interface IPersonService {
	List<Person> getAll();
}
