package ch.marcelhuber.pages;

import java.util.Locale;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.BeanEditForm;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ApplicationStateManager;
import org.apache.tapestry5.services.Cookies;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;

import ch.marcelhuber.model.EBelongTo;
import ch.marcelhuber.model.User;
import ch.marcelhuber.model.User.Role;
import ch.marcelhuber.utils.WeAreAppConstantLibrary;
import ch.marcelhuber.utils.WeAreAppUtil;


public class Login {

	@Inject
	private Logger logger;
	
	@Inject
	private Locale locale;
	
	@Inject
	private Cookies cookies;
	
	@Inject
	private Request request;
	
	@Inject
	private Messages messages;
	
	@Inject
	private PageRenderLinkSource pageRenderLS;
	
	@Inject
	private ApplicationStateManager applicationStateManager;

	@Property
	private User user;
	
	@Component
    private BeanEditForm form;
		
	void onPrepareForSubmit() throws Exception {
		user = new User();
	}

	Object onCanceled() {
		return Login.class;
	}

	void onValidateFromForm() {

		if (form.getHasErrors()) {
			return;
		}
		try {
			loginUser();
		} catch (Exception e) {
			form.recordError(e.getMessage());
		}
	}

	Object onSuccess() {
		return pageRenderLS.createPageRenderLinkWithContext(Index.class.getSimpleName(), EBelongTo.ALL);
	}
	
	private void loginUser() throws Exception {
		if(null == user) {
			String message = "User is null";
			logger.error(message);
			throw new Exception(message);
		}

		if( ! (("user".equals(user.getUserName()) && "1234".equals(user.getPassword()) || ("admin".equals(user.getUserName()) && "admin".equals(user.getPassword()))))) {
			String message = messages.get("login-password-wrong");
			logger.error(message + " for user: " + user.toString());
			throw new Exception(message);
		}
		
		if ("admin".equals(user.getUserName())) {
			user.setRole(Role.ADMIN);
		}
		
		cookies.getBuilder(WeAreAppConstantLibrary.COOKIE_USER, WeAreAppUtil.getMD5Hash(user.getUserName())).setMaxAge(WeAreAppConstantLibrary.COOKIE_MAX_AGE).write();
		applicationStateManager.set(User.class, user);
		logger.info("Login succes for" + user.toString());
	}
}
