package ch.marcelhuber.pages;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.slf4j.Logger;

import ch.marcelhuber.model.EBelongTo;
import ch.marcelhuber.model.Person;
import ch.marcelhuber.services.IPersonService;

public class Index {
	
	@Inject
	private Logger logger;	
	
	@Inject
	private PageRenderLinkSource pageRenderLS;
	
	@Inject
	private IPersonService personService;

	@Property
	private Person person;
	
	@Property
	private List<Person> persons;
	
	@Property
	private EBelongTo belongTo;
	
	Object onActivate() {
		if (belongTo == null) {
			return pageRenderLS.createPageRenderLinkWithContext(Index.class.getSimpleName(), EBelongTo.ALL);
		}
		return null;
	}
	
	Object onActivate(EBelongTo b) {
		
		if (b == null) {
			return pageRenderLS.createPageRenderLinkWithContext(Index.class.getSimpleName(), EBelongTo.ALL);
		}
		
		belongTo = b;
				
		boolean found = false;
		for (EBelongTo b2 : EBelongTo.values()) {
			if (b2.name().equals(belongTo.name())) {
				found = true;
				break;
			}
		}
		if ( ! found) {
			return Error404.class;
		}
		
		persons = personService.getAll();
		Collections.sort(persons, (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName()));
		if (belongTo != null && ! belongTo.name().equals(EBelongTo.ALL.name())) {
			persons = persons.stream().filter(e -> e.getBelongTo().equals(belongTo)).collect(Collectors.toList());
		}
		persons.stream().forEach(e -> logger.info(e.toString()));
		return null;
	}
	
	Object[] onPassivate() {
        return new Object[] { belongTo };
    }
}
