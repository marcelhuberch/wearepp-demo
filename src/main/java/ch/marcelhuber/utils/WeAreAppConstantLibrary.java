package ch.marcelhuber.utils;

public class WeAreAppConstantLibrary {
	
	public static final String COOKIE_USER = "weAreApp";
	public static final int COOKIE_MAX_AGE = 60 * 60 * 24 * 30;
}
