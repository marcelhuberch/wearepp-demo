package ch.marcelhuber.model;

import org.apache.tapestry5.beaneditor.DataType;
import org.apache.tapestry5.beaneditor.DataTypeConstants;

public class User {

	public enum Role {
		USER, ADMIN
	}

	private String userName;

	@DataType(DataTypeConstants.PASSWORD)
	private String password;

	private Role role = Role.USER;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [userName=" + userName + ", password=" + password + ", role=" + role + "]";
	}
}
