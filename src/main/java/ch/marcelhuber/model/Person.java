package ch.marcelhuber.model;

public class Person {

	private String name;
	private String description;
	private EBelongTo belongTo;
	private String image;
	private String email;
	private String phone;

	public Person(String name, String description, EBelongTo belongTo, String email, String phone, String image) {
		this.name = name;
		this.description = description;
		this.belongTo = belongTo;
		this.email = email;
		this.phone = phone;
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public EBelongTo getBelongTo() {
		return belongTo;
	}

	public void setBelongTo(EBelongTo belongTo) {
		this.belongTo = belongTo;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", description=" + description + ", belongTo=" + belongTo + ", image=" + image.length()
				+ ", email=" + email + ", phone=" + phone + "]";
	}
}
