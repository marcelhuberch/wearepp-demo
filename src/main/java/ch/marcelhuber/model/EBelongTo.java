package ch.marcelhuber.model;

public enum EBelongTo {

	ALL,
	HEAD_DEPARTMENT,
	IT_DEPARTMENT,
	FINANCE_DEPARTMENT,
	VISION_DEPARTMENT;
}
