package ch.marcelhuber.components;

import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;

import ch.marcelhuber.model.Person;

public class PersonComponent {
	
	@Parameter
	@Property
	private Person person;
}
