package ch.marcelhuber.components;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Cookies;
import org.slf4j.Logger;

import ch.marcelhuber.model.User;
import ch.marcelhuber.model.User.Role;
import ch.marcelhuber.pages.Admin;
import ch.marcelhuber.pages.Login;
import ch.marcelhuber.utils.WeAreAppConstantLibrary;

@Import(module = "bootstrap/collapse")
public class Layout {

	@Inject
	private Logger logger;
	
	@Inject
	private Cookies cookies;

	@SessionState
	@Property
	private User user;
	
	@Property
	private String adminPage = Admin.class.getSimpleName();

	@Property
	@Parameter(required = true, defaultPrefix = BindingConstants.LITERAL)
	private String title;

	Object onLogout() {
		logger.info("User logged out: " + user.toString());
		cookies.removeCookieValue(WeAreAppConstantLibrary.COOKIE_USER);
		user = null;
		return Login.class;
	}
	
	public boolean isAdmin() {
		if (user != null) {
			return Role.ADMIN.name().equals(user.getRole().name());
		}
		return false;
	}
}
