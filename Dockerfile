FROM tomcat:8.5-jre8
MAINTAINER marcelhuber

ADD target/weareapp.war /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run"]
